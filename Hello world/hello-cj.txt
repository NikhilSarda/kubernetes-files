apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello-cron-job
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello-cron-container
            image: ubuntu
            command:
            - "/bin/bash"
            - "-c"
            - "/bin/echo Hello from Pod $(Hostname) at $(date)"
          restartPolicy: Never